%% Task 1.8
close all;
clear all;

tstart=0;      %Sim start time
tstop=5000;    %Sim stop time
tsamp=1;      %Sampling time (NOT ODE solver time step)

p0=zeros(2,1); %Initial position (NED)
v0=[4 0]';  %Initial velocity (body)
psi0=0;        %Inital yaw angle
r0=0;          %Inital yaw rate
c=1;           %Current on (1)/off (0)

lw = 2;

%% Initialize rudder controller

T_psi = 63.9048;
K_psi = -0.0325;
ref = -10*pi/180;

wb = 0.037;
zeta = 0.8;
wn = wb/sqrt(1 - 2*zeta^2 + sqrt(4*zeta^4 - 4*zeta^2 + 2));

Kp_psi = (wn^2*T_psi)/K_psi;
Kd_psi = (2*zeta*wn*T_psi-1)/K_psi;
Ki_psi = (wn^3*T_psi)/(10*K_psi);

alpha_psi = 0.1;
Td_psi = Kd_psi/Kp_psi;
m_psi = T_psi/K_psi;

%% Initialize thruster controller

refStepTime = 700;
u_finalValue = 7;

dm = 0.002053;
C  = 0.001782;

syms 'ki'
syms 'kp'
r = roots([1 (dm-C*kp) C*ki]);

K_pu = 50;
K_iu = K_pu*dm*1.5;

s = tf('s');
closed_loop = (C*K_pu*s+C*K_iu)/(s^2 + (dm+C*K_pu)*s + C*K_iu);
[closedgain, closedphase] = bode(closed_loop, {0.00001 10});

open_loop = (K_pu+K_iu/s)*(C/(s+dm));
no_control = (C/(s+dm));
[opengain, openphase, openw] = bode(open_loop, {0.00001 10});
[nocgain, nocphase, nocw] = bode(no_control, {0.00001 10});
figure(1);clf(1);
hold on;
subplot(2, 1, 1);
semilogx(openw, 20*log(squeeze(opengain(1,1,:))), 'LineWidth', lw);
hold on;
semilogx(nocw, 20*log(squeeze(nocgain(1,1,:))), 'LineWidth', lw);
title('Bode plot with and without controller')
legend('With controller', 'No controller');
xlabel('Frequency [rad/s]');
ylabel('Gain [dB]')
subplot(2, 1, 2);
semilogx(openw, squeeze(openphase(1,1,:)), 'LineWidth', lw);
hold on;
semilogx(nocw, squeeze(nocphase(1,1,:)), 'LineWidth', lw);
legend('With controller', 'No controller');
xlabel('Frequency [rad/s]');
ylabel('Phase lag [dB]')



% omega_0ref = omega_0*0.05;
% zeta_ref = zeta*1.5;
omega_0ref = 0.02;
zeta_ref = 1.6;
%omega_0ref = sqrt(K_iu*C)
%zeta_ref = (dm+C*K_pu)/(2*omega_0ref);

%% Sim!

sim('MSFartoystyring');
u = v(:,1);

%% Plotting
figure(2);clf(2);
hold on;
grid on;
u_tilde = u-u_ref;
plot(t, u_tilde, 'LineWidth', lw);
title('Plot of the error u_{tilde}');
ylabel('Speed [m/s]');
xlabel('Time [s]');

figure(3);clf(3);
hold on; grid on;
plot(t, ref, 'LineWidth', lw);
plot(t,u_ref, 'LineWidth', lw);
plot(t, u, 'LineWidth', lw);
legend('r', 'u_{ref}', 'u');
title('Surge speed and reference');
ylabel('Speed [m/s]');
xlabel('Time [s]');

nc_max = 85*2*pi/60;
nc_actual =  min(nc_max, max(-nc_max, n_c));
figure(4);clf(4);
hold on; grid on;
plot(t, n_c, 'LineWidth', lw);
plot(t, nc_actual, 'LineWidth', lw);
legend('n_c', 'n_{c, actual}');