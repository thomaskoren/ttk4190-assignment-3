function psi_d = guidance2(pos, p0, p1) 

K_p = 1/350; % Lookahead distance

%Last waypoint
xp0 = p0(1,1);  
yp0 = p0(2,1);

%Next waypoint
xp1 = p1(1,1);
yp1 = p1(2,1);

%Current position
xs = pos(1,1);
ys = pos(2,1);


alphak = atan2(yp1-yp0,xp1-xp0); 

e = -(xs-xp0)*sin(alphak)+ (ys-yp0)*cos(alphak);
xi_r = atan2(-e*K_p,1);
psi_d = alphak + xi_r;

end


