function [psi_d] = heading_transform(chi, v)

U = sqrt(v(1,1)^2 + v(2,1)^2); % Speed of the ship
beta = asin(v(2,1)/U); % Sideslip angle
psi_d = chi - beta;  % Course to Yaw transformation

end

