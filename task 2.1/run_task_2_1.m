close all
clear all
load('WP.mat')
WP = WP';
%WP = rand(10,2)*10000;

%Calculate bezierPath;
bezierPath = createBezier(WP, 0.7);

%Calculate circle and straigh lines path
radii = [1000 1000 1000];
%radii = rand(1,8)*1500;
[circleCenters, circlePath] = createCirclePath(WP, radii);

%Plot control
pathColor = [0 0.4470 0.7410];
centerColor = [0.8500 0.3250 0.0980];
circColor = [0.9290 0.6940 0.1250];
wpColor = [0.6350 0.0780 0.1840];
pathWidth = 1.5;


%%Figure 1
figure(1);clf(1);
hold on;
subplot(3,1,1);
hold on;
grid on;
plot(bezierPath(:,1), bezierPath(:,2), 'LineWidth', pathWidth);
plot(WP(:,1), WP(:,2), '--', 'Color', pathColor);
plot(WP(:,1), WP(:,2), '*', 'Color', wpColor);
legend('Actual path', 'Straight path', 'Waypoints');

subplot(3,1,2);
hold on;
grid on;
plot(WP(:,1), WP(:,2), 'LineWidth', pathWidth);
plot(WP(:,1), WP(:,2), '*', 'Color', wpColor);
legend('Actual path' , 'Waypoints');

subplot(3,1,3)
hold on;
grid on;
angle = linspace(0, 2*pi, 360);
for i = 1:length(radii)
    r = radii(i);
    x = r*cos(angle)+circleCenters(i,1);
    y = r*sin(angle)+circleCenters(i,2);
    circ = plot(x, y, 'Color', circColor);
end
ap = plot(circleCenters(:,1), circleCenters(:,2), '.', 'Color', centerColor);
straight = plot(WP(:,1), WP(:,2), '--', 'Color', pathColor);
wp = plot(WP(:,1), WP(:,2), '*', 'Color', wpColor);
plot(circlePath(:, 1), circlePath(:,2), 'Color', pathColor, 'LineWidth', pathWidth);
legend([ap circ straight wp],{'Actual path', 'Circles', 'Straights', 'Waypoints'});


%%Figure 2
figure(2);clf(2);
hold on;
grid on;
plot(bezierPath(:,1), bezierPath(:,2), 'LineWidth', pathWidth);
plot(circlePath(:, 1), circlePath(:,2), 'LineWidth', pathWidth);
plot(WP(:,1), WP(:,2), 'LineWidth', pathWidth);
legend('Bezier curve', 'Circles and lines', 'Straight segments');
