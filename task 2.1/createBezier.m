function [path] = createBezier(waypoints, smoothness)
%Smoothness between 0 and 1
bezierpoints = [];
pathpoints = 100;
path = [waypoints(1,:)];
t = linspace(0,1,pathpoints)';
t = [t t];
for i=1:size(waypoints, 1)-2
    %Create bezier points
    point1 = waypoints(i,:)+(waypoints(i+1,:)-waypoints(i,:))*(1-0.5*smoothness);
    point2 = waypoints(i+1,:);
    point3 = waypoints(i+1,:)+(waypoints(i+2,:)-waypoints(i+1,:))*(0.5*smoothness);
    %Create path
    spline1 = (1-t).*repmat(point1, pathpoints, 1) + t.*repmat(point2, pathpoints, 1);
    spline2 = (1-t).*repmat(point2, pathpoints, 1)+ t.*repmat(point3, pathpoints, 1);
    spline3 = (1-t).*spline1 + t.*spline2;
    path = [path; spline3];
end
path = [path; waypoints(end,:)];
%Spread points with interpolation
path = interparc(10000, path(:,1), path(:,2), 'linear');
end