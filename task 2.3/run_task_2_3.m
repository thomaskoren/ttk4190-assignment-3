tstart=0;      %Sim start time
tstop=3000;    %Sim stop time
tsamp=10;      %Sampling time (NOT ODE solver time step)

p0=zeros(2,1); %Initial position (NED)
v0=[6.63 0]';  %Initial velocity (body)
psi0=0;        %Inital yaw angle
r0=0;          %Inital yaw rate
c=1;           %Current on (1)/off (0)

load('WP.mat')

% Heading autopilot
T = 137.1110;   % Time Nomoto's model parameter
K = -0.0553;    % Gain Nomoto's model parameter
wb = 0.015;     % Bandwidth
zeta = 1.1;     % Relative damping ratio

[Kp Ki Kd] = PID_compute_parameters(T, K, zeta, wb);
satDc = 0.44;   % Saturation of the rudder [rad]

% Surge controller
refSpeed = 7.2878*0.8;

dm = 0.002053;
C  = 0.001782;

K_pu = 50;
K_iu = K_pu*dm*1.5;

omega_0ref = 0.02;
zeta_ref = 1.6;

% Select the value for waypoint switching
switchingDistance = 1000;

sim MSFartoystyring

pathplotter(p(:,1), p(:,2), psi, tsamp, 10, tstart, tstop, 0, WP)