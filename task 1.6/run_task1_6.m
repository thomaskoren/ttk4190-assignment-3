% Task 1.6
% Parameter estimation

sysc.num = 0;
sysc.den = 1;

simulation = 0;
run

mdata = iddata(v(:,1),nc,tsamp);
sysid = armax(mdata,[1 1 0 0],tsamp);
systf = idtf(sysid);
sysc = d2c(systf);

sysc.num = sysc.num(end);
% Transfering the system from discreete to continous form increases the 
% order of the numerator. However as the terms are relatively small, only
% the last term of the numerator has been included in the reference model.
d_m = sysc.den(end)
C = sysc.num

simulation = 1;
run

%% Plots

figure(1)
clf(1)
hold on
plot(t,v(:,1), 'LineWidth', 2)
plot(t,u_est, 'LineWidth', 2)
legend('u','u_{est}')



