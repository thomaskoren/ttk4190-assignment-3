%% Task 1.4

%% Simulation parameters
tstart=0;      %Sim start time
tstop=3000;    %Sim stop time
tsamp=10;      %Sampling time (NOT ODE solver time step)

p0=zeros(2,1); %Initial position (NED)
v0=[6.63 0]';  %Initial velocity (body)
psi0=0;        %Inital yaw angle
r0=0;          %Inital yaw rate
c=1;           %Current on (1)/off (0)

T = 137.1110;   % Time Nomoto's model parameter
K = -0.0553;    % Gain Nomoto's model parameter
wb = 0.133;     % Bandwidth
zeta = 1.2;     % Relative damping ratio

%% Compute PID parameters
[Kp Ki Kd] = PID_compute_parameters(T, K, zeta, wb);

%% Simulation of the system
sim MSFartoystyring

%% Error signals 
error_psi = mean(psi-(psi_d.data))
error_r = mean(r-(r_d.data))

%% Plotting the results
figure(1);
plot(t,psi-(psi_d.data));
title(' Evolution of the error psi-psi_d ');
ylabel('psi-psi_d [rad]');
xlabel('Time [s]');

figure(2);
hold on;
plot(psi_d.time,psi_d.data,'r-');
plot(t,psi,'b-.');
title('Evolution of \psi_d and \psi');
ylabel('psi [rad]');
xlabel('Time [s]');
legend('\psi_d','\psi');

figure(3);
plot(t, r-r_d.data);
title('Evolution of the error r-r_d');
ylabel('r-r_d [rad]');
xlabel('Time [s]');

figure(4);
hold on;
plot(r_d.time,r_d.data,'r-');
plot(t,r,'b-.');
title('Evolution of r_d and r');
ylabel('r [rad]');
xlabel('Time [s]');
legend('r_d','r');





