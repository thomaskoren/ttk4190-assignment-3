function [Kp Ki Kd] = PID_compute_parameters(T, K, zeta, wb)
%This fuction computes the PID parameters for MS Fart�ystyring.
% Input: 
%   T, K : Nomoto first's model parameters
%   zeta : Relative damping ratio
%   wb   : Bandwidth
%
% Output: 
%   Kp = Proportional gain
%   Ki = Integral gain
%   Kd = Derivative gain

wn = wb/sqrt(1 - 2*zeta^2 + sqrt(4*zeta^4 - 4*zeta^2 + 2))     % Natural frequency

% PID controller
Kp = (wn^2*T)/K;    
Kd = (2*zeta*wn*T-1)/K;
Ki = (wn^3*T)/(10*K);


end

