function [vd, tarpos] = CBG(pt, vt, pa, offset)
% Returns the dersired interceptor approach velocity calculated via the
% Constant Bearing Gudance method
%
% Inputs:
%         pa = craft position
%         vt = target velocity
%         pt = target position
%         offset = offset from target position
% Output: vd = desired craft approach velocity

% Parameters
Ua_max = 4;     % max approach speed (absolute max speed U = 8.735)
deltap = 4000; % affects transient craft-target behaviour

%Rotating the offset vector
chi_r = atan2(-vt(2),-vt(1));
offset_rot = [cos(chi_r) -sin(chi_r); sin(chi_r) cos(chi_r)]*offset;

% Calculations
p = pa-pt+offset_rot;
if (norm(p) < 1e-1)
    p = [0; 0];
end
kappa = Ua_max*norm(p)/sqrt(p'*p+deltap^2);
va = -kappa*(p)/norm((p));
vd = vt+va;
tarpos = pt - offset_rot;
end