function [u_d] = speed_transform(v, U_d)

U = sqrt(v(1,1)^2 + v(2,1)^2); % The speed of the ship
beta = asin(v(2,1)/U); % Sideslip angle, eq. 10.85
u_d = U_d / cos(beta); % From standard triogometry


end

