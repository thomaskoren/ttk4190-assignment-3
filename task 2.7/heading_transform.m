function [psi_d] = heading_transform(chi, v)

U = sqrt(v(1,1)^2 + v(2,1)^2); % The speed of the ship
beta = asin(v(2,1)/U); % Sideslip angle, eq. 10.85
psi_d = chi - beta;

end

