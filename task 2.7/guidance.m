function psi_d = guidance(pos, p0, p1) 

K_p = 1/10000; % Lookahead distance

xp0 = p0(1,1);  % 
yp0 = p0(2,1);

xp1 = p1(1,1);
yp1 = p1(2,1);

xs = pos(1,1);
ys = pos(2,1);


alphak = atan2(yp1-yp0,xp1-xp0); 

e = -(xs-xp0)*sin(alphak)+ (ys-yp0)*cos(alphak);
xi_r = atan2(-e*K_p,1);
psi_d = alphak + xi_r;

end


