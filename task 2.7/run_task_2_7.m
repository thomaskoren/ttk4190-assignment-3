% run_task_2.7
%
% Target tracking
tstart=0;      %Sim start time
tstop=10000;    %Sim stop time
tsamp=10;      %Sampling time (NOT ODE solver time step)

p0=zeros(2,1); %Initial position (NED)
v0=[6.63 0]';  %Initial velocity (body)
psi0=0;        %Inital yaw angle
r0=0;          %Inital yaw rate
c=1;           %Current on (1)/off (0)

load('WP.mat')

T = 137.1110;   % Time Nomoto's model parameter
K = -0.0553;    % Gain Nomoto's model parameter
wb = 0.015;     % Bandwidth
zeta = 1.2;     % Relative damping ratio

% Rudder controller gains
[Kp Ki Kd] = PID_compute_parameters(T, K, zeta, wb);

%Surge controller gains
dm = 0.002053;
C  = 0.001782;

K_pu = 50;
K_iu = K_pu*dm*1.5;

%Surge reference model
omega_0ref = 0.02;
zeta_ref = 1.6;

%Tracking offset
offset = [0; -300];

satDc = 0.44;   % [radians] Saturation on the rudder angle

sim MSFartoystyring

pathplotter(p(:,1), p(:,2), psi, tsamp, 10, tstart, tstop, 1, WP)





